import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import theme from './theme';
import AppContent from './components/AppContent';


class App extends Component {
  render() {
    return (
        <MuiThemeProvider muiTheme={ theme }>
            <Router>
                <AppContent />
            </Router>
        </MuiThemeProvider>
    );
  }
}

export default App;
