import React, { Fragment } from 'react';
import { Switch } from 'react-router-dom';

import Routes from 'routes';

import AppBarWithDrawer from 'components/pieces/AppBarWithDrawer';
import { List, ListItem, ListLinkItem, ListDropdownItem } from 'components/pieces/List';


export default () => (
    <Fragment>
        <AppBarWithDrawer title='Sewwy'>
            {(handleItemClick) => (
                <List>
                    <ListItem primaryText='Close' onClick={ handleItemClick } />
                    <ListLinkItem to='/' primaryText='Home' onClick={ handleItemClick } />
                    <ListDropdownItem primaryText='Thread' nestedItems={[
                        <ListLinkItem to='/thread/sizes' key='sizes' primaryText='Sizes' onClick={ handleItemClick }/>
                    ]} />
                    <ListDropdownItem primaryText='Fabric' nestedItems={
                        [
                            'Knits'
                        ].map(
                            item => <ListItem key={ item } primaryText={ item } onClick={ handleItemClick }/>
                        )
                    } />
                </List>
            )}
        </AppBarWithDrawer>
        <Switch>
            <Routes />
        </Switch>
    </Fragment>
);
