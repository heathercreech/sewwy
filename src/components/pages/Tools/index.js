import React, { Fragment } from 'react';

import InfoPanel from 'components/pieces/InfoPanel';


export default () => (
    <Fragment>
        <InfoPanel
            title='Fabric Shears'
            description={`
                Cuts fabric.
                Don't use normal scissors, get fabric shears!
            `}
            accent
        />
        <InfoPanel
            title='Iron'
            description={`
                Irons stuff.
                Use a steam setting.
            `}
        />
        <InfoPanel
            title='Ironing Board'
            description={`
                Iron stuff on this.
                Surprised?
            `}
            accent
        />
        <InfoPanel
            title='Measuring Tape'
            description={`
                Measure stuff.
                120" normally, having a smaller one might help as well.
            `}
        />
        <InfoPanel
            title='Pins'
            description={`
                Holds stuff.
                Use glass pins if planning to iron a piece.
            `}
            accent
        />
        <InfoPanel
            title='Pin Holder'
            description={`
                Holds your pins.
                Ones wrist straps are probably super useful.
            `}
        />
        <InfoPanel
            title='Seam Ripper'
            description={`
                Rips seams, classic.
            `}
            accent
        />
        <InfoPanel
            title="Tailor's Chalk"
            description={`
                Mark your pieces for planning.
            `}
        />
    </Fragment>
);
