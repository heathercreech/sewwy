import React, { Fragment } from 'react';

import InfoPanel from 'components/pieces/InfoPanel';


export default () => (
    <Fragment>
        <InfoPanel
            title='Tex'
            description={`
            For home sewing (30, 45)
            Mass in grams per 1000 meters
            `}
            accent
        />
        <InfoPanel
            title='Denier'
            subtitle='(den or D)'
            description={`
                For home sewing (300, 460)
                Mass in grams per 9000 meters
            `}
        />
        <InfoPanel
            title='Commercial Sizes'
            subtitle='(V)'
            description={`
                Standard size system for marine grade thread
                For home sewing (V-30, V-46)
            `}
            accent
        />
    </Fragment>
);
