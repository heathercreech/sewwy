import React from 'react';

import { Grid } from 'components/pieces/Grid';
import { LinkTile } from 'components/pieces/Tile';


const Tile = (props) => <LinkTile size='300px' { ...props } />


export default () => (
    <Grid flex wrapItems>
        <Tile to='/tools' imageUrl='/images/spools.jpeg' title='Tools' />
        <Tile to='/thread/sizes' imageUrl='/images/thread-size.jpeg' title='Thread Size' />
    </Grid>
)
