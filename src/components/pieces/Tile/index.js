import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';



const StyledTile = styled.section`
    display: flex;
    justify-content: center;
    align-items: center;
    height: ${ props => props.size };
    width: ${ props => props.size };
    background-color: grey;
    background: radial-gradient(circle, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), transparent), url(${ props => props.imageUrl });
    background-size: cover;
    color: white;
    text-align: center;
    box-shadow: 0px 0px 1px black;
    margin: ${ ({ spacing }) => spacing ? spacing : '40px' };
    font-size: 32px;
    transition: font-size 0.1s, text-shadow 0.1s, opacity 0.5s;
    :hover{
        font-size: 36px;
        text-shadow: 1px 1px 5px black;
        opacity: 0.95;
    }
`;


export const Tile = ({ title, icon, ...rest }) => (
    <StyledTile { ...rest }>
        <div>
            <h3>{ title }</h3>
            <p>{ icon }</p>
        </div>
    </StyledTile>
)


export const LinkTile = ({ to, ...rest }) => (
    <Link to={ to } style={{ textDecoration: 'none' }}>
        <Tile { ...rest } />
    </Link>
)
