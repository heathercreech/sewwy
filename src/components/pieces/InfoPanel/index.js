import React from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';


const InfoPanel = ({ title, subtitle, description, muiTheme, accent=false }) => (
    <section style={{
        backgroundColor: (accent) ? muiTheme.palette.accent2Color : 'inherit',
        padding: '200px'
    }}>
        <h2 style={{ margin: 0 }}>
            { title }
            <span style={{
                fontSize: '16px',
                marginLeft: '10px',
                color: muiTheme.palette.secondaryTextColor,
                display: (subtitle) ? 'inline' : 'none'
            }}>
                { subtitle }
            </span>
        </h2>
        <p>
            { description }
        </p>
    </section>
);

export default muiThemeable()(InfoPanel)
