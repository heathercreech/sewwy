import styled from 'styled-components';


export const Grid = styled.section`
    display: ${ props => props.flex ? 'flex' : 'grid' };
    flex-wrap: ${ props => props.wrapItems ? 'wrap' : '' };
    justify-content: center;
    align-items: center;
    grid-template-columns: ${ props => '1fr '.repeat(props.columns) };
    grid-template-rows: ${ props => '1fr '.repeat(props.rows) };
`;
