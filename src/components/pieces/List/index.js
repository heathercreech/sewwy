import React from 'react';
import { List as MuiList, ListItem as MuiListItem} from 'material-ui/List';
import { Link, withRouter } from 'react-router-dom'


export const List = (props) => <MuiList { ...props } />;


export const ListItem = (props) => (
    <MuiListItem
        style={{
            textAlign: 'left',
            userSelect: 'none'
        }}
        { ...props }
    />
);


export const ListLinkItem = withRouter(({ history, staticContext, to, ...rest }) => (
    <Link style={{textDecoration: 'none'}} to={to} ><ListItem { ...rest } /></Link>
));


export const ListDropdownItem = ({ nestedItems, ...rest }) => (
    <ListItem
        primaryTogglesNestedList={ true }
        nestedItems={ nestedItems }
        { ...rest }
    />
);
