import React, { Fragment, Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';


export default class AppBarWithDrawer extends Component {

    state = {
        drawerOpen: false
    }

    handleOpen = () => this.setState({
        drawerOpen: true
    })

    handleRequestChange = (open) => this.setState({
        drawerOpen: open
    })

    handleItemClick = () => this.setState({
        drawerOpen: false
    })

    render() {
        let { title, children } = this.props;
        let { drawerOpen } = this.state;
        return (
            <Fragment>
                <AppBar
                    title={ title }
                    onLeftIconButtonClick={ this.handleOpen }
                />
                <Drawer
                    docked={ false }
                    open={ drawerOpen }
                    onRequestChange={ this.handleRequestChange }
                >
                    { children(this.handleItemClick) }
                </Drawer>
            </Fragment>
        )
    }
}
