import {
    //deepOrange500, deepOrange700
    grey900, grey800
} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

export default getMuiTheme({
    palette: {
        primary1Color: grey800,
        primary2Color: grey900,
        pickerHeaderColor: grey800
    }
});
