import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import { CSSTransitionGroup } from 'react-transition-group';

import Home from 'components/pages/Home';
import Tools from 'components/pages/Tools';
import ThreadSize from 'components/pages/ThreadSize';


const routeList = [
    {path: '/', component: Home, exact: true},
    {path: '/tools', component: Tools},
    {path: '/thread/sizes', component: ThreadSize},
]

const FadeRoute = ({ Component, ...rest }) => (
    <Route { ...rest }>
        { () => (
        <CSSTransitionGroup
            transitionName='switch'
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={false}
            transitionLeave={false}
        >
            <Component key={Date.now()} />
        </CSSTransitionGroup>
        ) }
    </Route>
)


export default () => (
    <Fragment>
        <style>
        {".switch-appear {   opacity: 0.01; }  .switch-enter.switch-appear-active {   opacity: 1;   transition: opacity 500ms ease-in; }"}
        </style>
        { routeList.map(route => (
            <FadeRoute key={ route.path } { ...route } />
        )) }
    </Fragment>
)


