const rewireHotLoader = require('react-app-rewire-hot-loader');

module.exports = function override(config, env) {
    config = rewireHotLoader(config, env);
    return config;
}
